﻿using FluentValidation;
using Nop.Core.Domain.Institutes;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.LanguageInstitute;
using Nop.Web.Framework.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Validators.Institutes
{
    public class LanguageInstituteTermValidator : BaseNopValidator<LanguageInstituteTermModel>
    {
        public LanguageInstituteTermValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.LangName).NotEmpty().WithMessage(localizationService.GetResource("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.Title.Required"));

            RuleFor(x => x.LangCost).GreaterThan(0).WithMessage(localizationService.GetResource("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangCost.GreaterThanZero"));

            SetDatabaseValidationRules<LanguageInstituteCourse>(dbContext);
        }
    }
}
