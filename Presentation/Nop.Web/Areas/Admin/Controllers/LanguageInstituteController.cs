﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Institutes;
using Nop.Services.Events;
using Nop.Services.Institutes;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Extensions;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.LanguageInstitute;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using System;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class LanguageInstituteController : BaseAdminController
    {
        #region Fields

        private readonly ICustomerActivityService _customerActivityService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IInstituteModelFactory _instituteModelFactory;
        private readonly ILanguageInstituteTermService _instituteTermService;
        private readonly IPermissionService _permissionService;

        #endregion

        #region Ctor

        public LanguageInstituteController(ICustomerActivityService customerActivityService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IInstituteModelFactory instituteModelFactory,
            ILanguageInstituteTermService instituteTermService,
            IPermissionService permissionService)
        {
            this._customerActivityService = customerActivityService;
            this._eventPublisher = eventPublisher;
            this._localizationService = localizationService;
            this._instituteModelFactory = instituteModelFactory;
            this._instituteTermService = instituteTermService;
            this._permissionService = permissionService;
        }

        #endregion

        #region Methods
        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }
        
        public virtual IActionResult LanguageInstituteCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageLanguageInstitute))
                return AccessDeniedView();

            //prepare model
            var model = _instituteModelFactory.PrepareInstituteTermModel(new LanguageInstituteTermModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult LanguageInstituteCreate(LanguageInstituteTermModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageLanguageInstitute))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var instituteTerm = model.ToEntity<LanguageInstituteCourse>();
                instituteTerm.LangCreationDate = DateTime.Now;
                _instituteTermService.InsertLanguageInstituteTerm(instituteTerm);

                SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Added"));

                if (!continueEditing)
                    return RedirectToAction("List");

                //activity log
                _customerActivityService.InsertActivity("AddNewInstituteTerm",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewInstituteTerm"), instituteTerm.Id), instituteTerm);

                return RedirectToAction("LanguageInstituteEdit", new { id = instituteTerm.Id });
            }

            //prepare model
            model = _instituteModelFactory.PrepareInstituteTermModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion
    }
}