﻿using Nop.Core.Domain.Institutes;
using Nop.Web.Areas.Admin.Models.LanguageInstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial interface IInstituteModelFactory
    {
        LanguageInstituteTermModel PrepareInstituteTermModel(LanguageInstituteTermModel model, LanguageInstituteCourse newsItem, bool excludeProperties = false);
    }
}
