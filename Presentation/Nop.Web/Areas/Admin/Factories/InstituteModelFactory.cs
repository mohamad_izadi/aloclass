﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core.Domain.Institutes;
using Nop.Web.Areas.Admin.Models.LanguageInstitute;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Institutes;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial class InstituteModelFactory : IInstituteModelFactory
    {
        #region Fields

        private readonly ILangService _langService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public InstituteModelFactory(ILangService langService,
            ILocalizationService localizationService)
        {
            this._langService = langService;
            this._localizationService = localizationService;
        }

        #endregion

        #region Methods

        public virtual LanguageInstituteTermModel PrepareInstituteTermModel(LanguageInstituteTermModel model, LanguageInstituteCourse instituteTerm, bool excludeProperties = false)
        {
            //fill in model values from the entity
            if (instituteTerm != null)
            {
                model = model ?? instituteTerm.ToModel<LanguageInstituteTermModel>();
            }

            model.AvailableLanguages.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });

            foreach (var s in _langService.GetAllLanguages())
            {
                model.AvailableLanguages.Add(new SelectListItem
                {
                    Text = s.Title_Fa,
                    Value = s.Id.ToString(),
                    Selected = s.Id == model.Id
                });
            }

            return model;
        }

        #endregion
    }
}
