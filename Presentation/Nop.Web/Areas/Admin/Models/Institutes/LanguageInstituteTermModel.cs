﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Validators.Institutes;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.LanguageInstitute
{
    [Validator(typeof(LanguageInstituteTermValidator))]
    public partial class LanguageInstituteTermModel : BaseNopEntityModel
    {
        public LanguageInstituteTermModel()
        {
            AvailableLanguages = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.Language")]
        public IList<SelectListItem> AvailableLanguages { get; set; }

        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.Language")]
        public string LangName { get; set; }

        [NopResourceDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.CustomerRoles")]
        public List<string> AvailableWeekDays { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangWeeklyDays")]
        public int[] LangWeeklyDays { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangWeeklyHours")]
        public string LangWeeklyHours { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangPrerequisite")]
        public string LangPrerequisite { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangCost")]
        public decimal LangCost { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangDescription")]
        public string LangDescription { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangContacts")]
        public string LangContacts { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangCapacity")]
        public string LangCapacity { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangSexuality")]
        public int LangSexuality { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangSalaryMethod")]
        public int LangSalaryMethod { get; set; }
    }
}
