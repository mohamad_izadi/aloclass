﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.LanguageInstitute
{
    public partial class LanguageInstituteValidator : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangCreationYear")]
        public int LangCreationYear { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangRank")]
        public int LangRank { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.LanguageInstitute.LanguageInstituteTerm.Fields.LangAddress")]
        public string LangAddress { get; set; }
    }
}
