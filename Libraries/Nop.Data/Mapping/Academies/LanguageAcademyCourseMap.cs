using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Academies;

namespace Nop.Data.Mapping.Academies
{
    /// <summary>
    /// Represents a customer mapping configuration
    /// </summary>
    public partial class LanguageAcademyCourseMap : NopEntityTypeConfiguration<LanguageAcademyCourse>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<LanguageAcademyCourse> builder)
        {
            builder.ToTable("AcademyLanguageCourse");
            builder.HasKey(languageAcademyCourse => languageAcademyCourse.Id);

            builder.Property(languageAcademyCourse => languageAcademyCourse.LanguageId).IsRequired();
            builder.Property(languageAcademyCourse => languageAcademyCourse.DaysInWeek).HasMaxLength(255);
            builder.Property(languageAcademyCourse => languageAcademyCourse.HoursInWeek).HasMaxLength(255);
            builder.Property(languageAcademyCourse => languageAcademyCourse.Fee).IsRequired();
            builder.Property(languageAcademyCourse => languageAcademyCourse.Overview).HasMaxLength(255);
            builder.Property(languageAcademyCourse => languageAcademyCourse.AgeCategory).HasMaxLength(50);

            //builder.Ignore(customer => customer.CustomerRoles);
            //builder.Ignore(customer => customer.Addresses);

            base.Configure(builder);
        }

        #endregion
    }
}