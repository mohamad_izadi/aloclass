using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Academies;

namespace Nop.Data.Mapping.Academies
{
    /// <summary>
    /// Represents a customer mapping configuration
    /// </summary>
    public partial class LanguageAcademyMap : NopEntityTypeConfiguration<LanguageAcademy>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<LanguageAcademy> builder)
        {
            builder.ToTable("AcademyLanguage");
            builder.HasKey(languageAcademy => languageAcademy.Id);

            builder.Property(languageAcademy => languageAcademy.Name).IsRequired().HasMaxLength(255);
            builder.Property(languageAcademy => languageAcademy.Motto).HasMaxLength(255);
            builder.Property(languageAcademy => languageAcademy.EstablishmentYear).IsRequired().HasMaxLength(4);
            
            //builder.Ignore(customer => customer.CustomerRoles);
            //builder.Ignore(customer => customer.Addresses);

            base.Configure(builder);
        }

        #endregion
    }
}