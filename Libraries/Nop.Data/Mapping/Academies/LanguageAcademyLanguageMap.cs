using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Academies;

namespace Nop.Data.Mapping.Academies
{
    /// <summary>
    /// Represents a customer mapping configuration
    /// </summary>
    public partial class LanguageAcademyLanguageMap : NopEntityTypeConfiguration<LanguageAcademyLanguage>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<LanguageAcademyLanguage> builder)
        {
            builder.ToTable("AcademyLanguageLanguage");
            builder.HasKey(languageAcademyLanguage => languageAcademyLanguage.Id);

            builder.Property(languageAcademyLanguage => languageAcademyLanguage.TitleOriginal)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(languageAcademyLanguage => languageAcademyLanguage.TitleFarsi).HasMaxLength(255);
            

            //builder.Ignore(customer => customer.CustomerRoles);
            //builder.Ignore(customer => customer.Addresses);

            base.Configure(builder);
        }

        #endregion
    }
}