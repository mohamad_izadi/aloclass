﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Institutes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Institutes
{
    public partial class LanguageInstituteMap : NopEntityTypeConfiguration<LanguageInstitute>
    {
        public override void Configure(EntityTypeBuilder<LanguageInstitute> builder)
        {
            builder.ToTable(nameof(LanguageInstitute));
            builder.HasKey(institute => institute.Id);
            
            base.Configure(builder);
        }
    }
}
