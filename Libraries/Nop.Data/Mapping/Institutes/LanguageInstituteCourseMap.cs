﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Institutes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Institutes
{
    public partial class LanguageInstituteCourseMap : NopEntityTypeConfiguration<LanguageInstituteCourse>
    {
        public override void Configure(EntityTypeBuilder<LanguageInstituteCourse> builder)
        {
            builder.ToTable(nameof(LanguageInstituteCourse));
            builder.HasKey(it => it.Id);

            base.Configure(builder);
        }
    }
}
