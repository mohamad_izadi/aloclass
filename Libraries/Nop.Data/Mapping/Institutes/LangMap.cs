﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Institutes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Institutes
{
    public partial class LangMap : NopEntityTypeConfiguration<Lang>
    {
        public override void Configure(EntityTypeBuilder<Lang> builder)
        {
            builder.ToTable(nameof(Lang));
            builder.HasKey(it => it.Id);

            base.Configure(builder);
        }
    }
}
