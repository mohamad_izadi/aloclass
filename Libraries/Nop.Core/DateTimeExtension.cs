﻿using System;

namespace Nop.Core
{
    public static class DateTimeExtension
    {
        public static string ToPerianDateTimeFormat(this DateTime date, string format = "yyyy/MM/dd")
        {
            return new PersianDateTime(date) { EnglishNumber = true }.ToString(format);
        }
        public static string ToPersianLongDateTimeString(this DateTime date)
        {
            return new PersianDateTime(date) { EnglishNumber = true }.ToString("dddd، dd MMMM yyyy hh:mm:ss tt");
        }

        public static string ToPersianLongTimeFormat(this DateTime date)
        {
            return new PersianDateTime(date) { EnglishNumber = true }.ToString("hh:mm:ss tt");
        }

        public static string ToPersianLongDateFormat(this DateTime date)
        {
            return new PersianDateTime(date) { EnglishNumber = true }.ToString("dddd، yyyy/MM/dd");
        }

        public static string ToPersianDateTimeString(this DateTime date)
        {
            return new PersianDateTime(date) { EnglishNumber = true }.ToString("yyyy/MM/dd hh:mm:ss tt");
        }

        public static DateTime ToDateTime(this string date)
        {
            PersianDateTime value;
            if (PersianDateTime.TryParse(date, out value))
                return value.ToDateTime();
            else
                throw new FormatException("DateTime format is invalid !");
        }
        public static PersianDateTime ToPersianDateTime(this DateTime date)
        {
            return new PersianDateTime(date) { EnglishNumber = true };
        }
    }
}
