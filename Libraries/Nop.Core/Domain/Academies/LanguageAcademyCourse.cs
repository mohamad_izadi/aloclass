using System;

namespace Nop.Core.Domain.Academies
{
    /// <summary>
    /// Represents a academy of
    /// </summary>
    public partial class LanguageAcademyCourse : BaseEntity
    {
        public LanguageAcademyCourse()
        {
        }

        /// <summary>
        /// Gets or sets the course's language
        /// </summary>
        public int LanguageId { get; set; }

        /// <summary>
        /// Gets or sets days which the course is held in.
        /// </summary>
        public string DaysInWeek { get; set; }

        /// <summary>
        /// Gets or sets hours which the course is held in.
        /// </summary>
        public string HoursInWeek { get; set; }

        /// <summary>
        /// Gets or sets number of sessions will be held in the course.
        /// </summary>
        public int SessionCount { get; set; }

        /// <summary>
        /// Gets or sets prerequisites of the course.
        /// </summary>
        public string Prerequisite { get; set; }

        /// <summary>
        /// Gets or sets course's fee.
        /// </summary>
        public decimal Fee { get; set; }

        /// <summary>
        /// Gets or sets Overview of the course.
        /// </summary>
        public string Overview { get; set; }

        /// <summary>
        /// Gets or sets Description of the course.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets age category of the course.
        /// </summary>
        public string AgeCategory { get; set; }

        /// <summary>
        /// Gets or sets capacity of the course.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets age gender of the course's contacts.
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// Gets or sets cash or installment of the fee payment using enum type
        /// </summary>
        public int FeePaymentTypeId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the language has been published
        /// </summary>
        public bool Published { get; set; }

        #region Navigation properties
        #endregion
    }
}