using System;

namespace Nop.Core.Domain.Academies
{
    /// <summary>
    /// Represents a academy of
    /// </summary>
    public partial class LanguageAcademy : BaseAcademy
    {
        public LanguageAcademy()
        {
        }

        /// <summary>
        /// Gets or sets the AcademyId
        /// </summary>
        public int AcademyType { get; set; }

        /// <summary>
        /// Gets or sets the JobTitleId
        /// </summary>
        public int JobTitleId { get; set; }
                
        /// <summary>
        /// Gets or sets the telephone number
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// Gets or sets the mobile phone number
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the user image
        /// </summary>
        public int PictureId { get; set; }


        #region Navigation properties
        #endregion
    }
}