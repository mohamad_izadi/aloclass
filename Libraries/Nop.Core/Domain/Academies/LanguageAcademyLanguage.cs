using System;

namespace Nop.Core.Domain.Academies
{
    /// <summary>
    /// Represents a academy of
    /// </summary>
    public partial class LanguageAcademyLanguage : BaseEntity
    {
        public LanguageAcademyLanguage()
        {
        }

        /// <summary>
        /// Gets or sets the academy's lang original title
        /// </summary>
        public string TitleOriginal { get; set; }

        /// <summary>
        /// Gets or sets the academy's lang farsi title
        /// </summary>
        public string TitleFarsi { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the language has been published
        /// </summary>
        public bool Published { get; set; }

        #region Navigation properties
        #endregion
    }
}