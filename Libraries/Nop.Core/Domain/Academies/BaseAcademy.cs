using System;

namespace Nop.Core.Domain.Academies
{
    /// <summary>
    /// Represents a base academy
    /// </summary>
    public abstract partial class BaseAcademy : BaseEntity
    {          
        /// <summary>
        /// Gets or sets the academy name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the acedemy overview
        /// </summary>
        public string Motto { get; set; }

        /// <summary>
        /// Gets or sets the acedemy overview
        /// </summary>
        public string Overview { get; set; }

        /// <summary>
        /// Gets or sets the academy description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the academy establishment year
        /// </summary>
        public string EstablishmentYear { get; set; }

        /// <summary>
        /// Gets or sets the academy overall score attained from Clients
        /// </summary>
        public float CustomerOverallScore { get; set; }

        /// <summary>
        /// Gets or sets the academy overall score attained from AloKelas
        /// </summary>
        public float HolderOverallScore { get; set; }

        /// <summary>
        /// Gets or sets the academy number of employees
        /// </summary>
        public int EmployeeCount { get; set; }

        /// <summary>
        /// Gets or sets the academy type of fee payment
        /// </summary>
        public int FeePaymentTypeId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the academy is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the academy has been deleted
        /// </summary>
        public bool Deleted { get; set; }
                
        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the language has been published
        /// </summary>
        public bool Published { get; set; }

        #region Navigation properties

        // List of academy pictures will be added here.
        // List of addresses including tels, emails and addresses will be added here.

        #endregion
    }
}