﻿using System.ComponentModel;

namespace Nop.Core.Domain.Academies
{
    /// <summary>
    /// Represents the customer registration type formatting enumeration
    /// </summary>
    public enum AcademyFeePaymentType
    {
        /// <summary>
        /// Cash payment
        /// </summary>
        [Description("پرداخت نقدی")]
        CashPayment = 1,

        /// <summary>
        /// Installment payment
        /// </summary>
        [Description("پرداخت اقساطی")]
        InstallmentPayment = 2,
    }
}
