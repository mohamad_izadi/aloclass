﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Institutes
{
    public partial class LanguageInstitute : BaseEntity
    {
        public string Title { get; set; }
        public int EstablishedYear { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime? UpdateOnUtc { get; set; }
        public bool Published { get; set; }
    }
}
