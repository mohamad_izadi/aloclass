﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Institutes
{
    public enum LanguageEnum
    {
        English = 0,
        German = 1,
        French = 2,
        Turkish = 3,
    }
}
