﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Institutes
{
    public partial class Lang : BaseEntity
    {
        public string Title_En { get; set; }
        public string Title_Fa { get; set; }
        public bool Published { get; set; }
    }
}
