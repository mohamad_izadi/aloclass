﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Institutes
{
    public partial class LanguageInstituteCourse : BaseEntity
    {
        public string LangName { get; set; }
        public int LangCreationYear { get; set; }
        public int LangRank { get; set; }
        public string LangAddress { get; set; }
        public int LangWeeklyDays { get; set; }
        public string LangWeeklyHours { get; set; }
        public string LangPrerequisite { get; set; }
        public decimal LangCost { get; set; }
        public string LangDescription { get; set; }
        public string LangContacts { get; set; }
        public string LangCapacity { get; set; }
        public int LangSexuality { get; set; }
        public int LangSalaryMethod { get; set; }
        public DateTime LangCreationDate { get; set; }
        public DateTime? LangEditDate { get; set; }
    }
}
