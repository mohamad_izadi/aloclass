using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Common;

namespace Nop.Core.Domain.Personnels
{
    /// <summary>
    /// Represents a customer
    /// </summary>
    public partial class AcademyForeignLanguages : BaseEntity
    {
        public AcademyForeignLanguages()
        {
        }
                
        /// <summary>
        /// Gets or sets the personnel's AcademyId
        /// </summary>
        public int AcademyId { get; set; }

        /// <summary>
        /// Gets or sets the type of personnel's Academy
        /// </summary>
        public int AcademyType { get; set; }

        /// <summary>
        /// Gets or sets the personnelJobTitleId using enum type.
        /// </summary>
        public int JobTitleId { get; set; }

        /// <summary>
        /// Gets or sets the personnel's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the telephone number
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// Gets or sets the mobile phone number
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the user image
        /// </summary>
        public int PictureId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer has been deleted
        /// </summary>
        public bool Deleted { get; set; }
                
        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the language has been published
        /// </summary>
        public bool Published { get; set; }
        
        #region Navigation properties
        #endregion
    }
}