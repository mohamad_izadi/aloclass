﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Institutes;
using Nop.Services.Events;

namespace Nop.Services.Institutes
{
    public partial class LangService : ILangService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Lang> _langRepository;
        private readonly string _entityName;

        #endregion

        #region Ctor

        public LangService(
            IEventPublisher eventPublisher,
            IRepository<Lang> langRepository)
        {
            this._eventPublisher = eventPublisher;
            this._langRepository = langRepository;
            this._entityName = typeof(Lang).Name;
        }

        #endregion

        public IPagedList<Lang> GetAllLanguages(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _langRepository.Table;
            if (!showHidden)
            {
                query = query.Where(n => n.Published);
            }

            var langs = new PagedList<Lang>(query, pageIndex, pageSize);
            return langs;
        }

        public Lang GetLanguageById(int langId)
        {
            if (langId == 0)
                return null;

            return _langRepository.GetById(langId);
        }

        public IList<Lang> GetLanguagesByIds(int[] langIds)
        {
            var query = _langRepository.Table;
            return query.Where(p => langIds.Contains(p.Id)).ToList();
        }

        public void InsertLanguage(Lang lang)
        {
            throw new NotImplementedException();
        }

        public void UpdateLanguage(Lang lang)
        {
            throw new NotImplementedException();
        }

        public void DeleteLanguage(Lang lang)
        {
            throw new NotImplementedException();
        }
    }
}
