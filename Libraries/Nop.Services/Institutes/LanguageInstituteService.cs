﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Institutes;
using Nop.Services.Events;
using System.Linq;

namespace Nop.Services.Institutes
{
    public partial class LanguageInstituteService : ILanguageInstituteService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<LanguageInstitute> _instituteRepository;
        private readonly string _entityName;

        #endregion

        #region Ctor

        public LanguageInstituteService(
            IEventPublisher eventPublisher,
            IRepository<LanguageInstitute> instituteRepository)
        {
            this._eventPublisher = eventPublisher;
            this._instituteRepository = instituteRepository;
            this._entityName = typeof(LanguageInstitute).Name;
        }

        #endregion

        #region Methods
        public IPagedList<LanguageInstitute> GetAllLanguageInstitutes(int languageId = 0, int storeId = 0, int pageIndex = 0,
            int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _instituteRepository.Table;
            if (!showHidden)
            {
                query = query.Where(n => n.Published);
            }

            var institutes = new PagedList<LanguageInstitute>(query, pageIndex, pageSize);
            return institutes;
        }

        public LanguageInstitute GetLanguageInstituteById(int instituteId)
        {
            if (instituteId == 0)
                return null;

            return _instituteRepository.GetById(instituteId);
        }

        public IList<LanguageInstitute> GetLanguageInstituteByIds(int[] instituteIds)
        {
            var query = _instituteRepository.Table;
            return query.Where(p => instituteIds.Contains(p.Id)).ToList();
        }

        public void InsertLanguageInstitute(LanguageInstitute institute)
        {
            if (institute == null)
                throw new ArgumentNullException(nameof(institute));

            _instituteRepository.Insert(institute);

            //event notification
            _eventPublisher.EntityInserted(institute);
        }

        public void UpdateLanguageInstitute(LanguageInstitute institute)
        {
            if (institute == null)
                throw new ArgumentNullException(nameof(institute));

            _instituteRepository.Update(institute);

            //event notification
            _eventPublisher.EntityUpdated(institute);
        }

        public void DeleteLanguageInstitute(LanguageInstitute institute)
        {
            if (institute == null)
                throw new ArgumentNullException(nameof(institute));

            _instituteRepository.Delete(institute);

            //event notification
            _eventPublisher.EntityDeleted(institute);
        }

        #endregion
    }
}
