﻿using Nop.Core;
using Nop.Core.Domain.Institutes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.Institutes
{
    public partial interface ILanguageInstituteService
    {
        #region Institute
        
        LanguageInstitute GetLanguageInstituteById(int instituteId);
        
        IList<LanguageInstitute> GetLanguageInstituteByIds(int[] instituteIds);
        
        IPagedList<LanguageInstitute> GetAllLanguageInstitutes(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);
        
        void InsertLanguageInstitute(LanguageInstitute institute);
        
        void UpdateLanguageInstitute(LanguageInstitute institute);

        void DeleteLanguageInstitute(LanguageInstitute institute);

        #endregion
    }
}
