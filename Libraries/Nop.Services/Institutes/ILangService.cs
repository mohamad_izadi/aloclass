﻿using Nop.Core;
using Nop.Core.Domain.Institutes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.Institutes
{
    public partial interface ILangService
    {
        Lang GetLanguageById(int langId);

        IList<Lang> GetLanguagesByIds(int[] langIds);

        IPagedList<Lang> GetAllLanguages(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        void InsertLanguage(Lang lang);

        void UpdateLanguage(Lang lang);

        void DeleteLanguage(Lang lang);
    }
}
