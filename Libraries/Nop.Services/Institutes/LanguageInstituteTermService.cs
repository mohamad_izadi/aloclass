﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Institutes;
using Nop.Services.Events;
using System.Linq;

namespace Nop.Services.Institutes
{
    public partial class LanguageInstituteTermService : ILanguageInstituteTermService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<LanguageInstituteCourse> _instituteItemRepository;
        private readonly string _entityName;

        #endregion

        #region Ctor

        #region Methods

        public LanguageInstituteTermService(
            IEventPublisher eventPublisher,
            IRepository<LanguageInstituteCourse> instituteItemRepository)
        {
            this._eventPublisher = eventPublisher;
            this._instituteItemRepository = instituteItemRepository;
            this._entityName = typeof(LanguageInstituteCourse).Name;
        }

        public void DeleteLanguageInstituteTerm(LanguageInstituteCourse instituteTerm)
        {
            if (instituteTerm == null)
                throw new ArgumentNullException(nameof(instituteTerm));

            _instituteItemRepository.Delete(instituteTerm);

            //event notification
            _eventPublisher.EntityDeleted(instituteTerm);
        }

        public IPagedList<LanguageInstituteCourse> GetAllLanguageInstituteTerms(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _instituteItemRepository.Table;

            var instituteTerms = new PagedList<LanguageInstituteCourse>(query, pageIndex, pageSize);
            return instituteTerms;
        }

        public LanguageInstituteCourse GetLanguageInstituteTermById(int instituteTermId)
        {
            if (instituteTermId == 0)
                return null;

            return _instituteItemRepository.GetById(instituteTermId);
        }

        public IList<LanguageInstituteCourse> GetLanguageInstituteTermsByIds(int[] instituteTermIds)
        {
            var query = _instituteItemRepository.Table;
            return query.Where(p => instituteTermIds.Contains(p.Id)).ToList();
        }

        public void InsertLanguageInstituteTerm(LanguageInstituteCourse instituteTerm)
        {
            if (instituteTerm == null)
                throw new ArgumentNullException(nameof(instituteTerm));

            _instituteItemRepository.Insert(instituteTerm);

            //event notification
            _eventPublisher.EntityInserted(instituteTerm);
        }

        public void UpdateLanguageInstituteTerm(LanguageInstituteCourse instituteTerm)
        {
            if (instituteTerm == null)
                throw new ArgumentNullException(nameof(instituteTerm));

            _instituteItemRepository.Update(instituteTerm);

            //event notification
            _eventPublisher.EntityUpdated(instituteTerm);
        }

        #endregion

        #endregion
    }
}
