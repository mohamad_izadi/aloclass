﻿using Nop.Core;
using Nop.Core.Domain.Institutes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.Institutes
{
    public partial interface ILanguageInstituteTermService
    {
        #region Language Institute term

        LanguageInstituteCourse GetLanguageInstituteTermById(int instituteTermId);
        
        IList<LanguageInstituteCourse> GetLanguageInstituteTermsByIds(int[] instituteTermsIds);
        
        IPagedList<LanguageInstituteCourse> GetAllLanguageInstituteTerms(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);
        
        void InsertLanguageInstituteTerm(LanguageInstituteCourse instituteTerm);
        
        void UpdateLanguageInstituteTerm(LanguageInstituteCourse instituteTerm);

        void DeleteLanguageInstituteTerm(LanguageInstituteCourse instituteTerm);

        #endregion
    }
}
